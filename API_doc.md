# Backend API Documentation

This document provides the details of the backend API for the frontend-handover process, ensuring a smooth integration for frontend developers.

## Base URL

The base URL for all API endpoints is `https://28-api.app.softgen.ai/`

## API Endpoints

### Todo Endpoints

#### Create a Todo

- **URL**: `https://28-api.app.softgen.ai/api/todos`
- **Method**: `POST`
- **Request Body**: 
  ```json
  {
    "title": "string",
    "description": "string",
    "priority": "low | medium | high",
    "status": "pending | completed",
    "tags": ["string"]
  }
  ```
- **Response**: 
  ```json
  {
    "id": "number",
    "title": "string",
    "description": "string",
    "priority": "string",
    "status": "string",
    "tags": ["string"],
    "createdAt": "datetime",
    "updatedAt": "datetime"
  }
  ```
- **Status Codes**: 
  - `201 Created` on success
  - `500 Internal Server Error` on failure

#### Get All Todos

- **URL**: `https://28-api.app.softgen.ai/api/todos`
- **Method**: `GET`
- **Response**: 
  ```json
  [
    {
      "id": "number",
      "title": "string",
      "description": "string",
      "priority": "string",
      "status": "string",
      "tags": ["string"],
      "createdAt": "datetime",
      "updatedAt": "datetime"
    }
  ]
  ```
- **Status Codes**: 
  - `200 OK` on success
  - `500 Internal Server Error` on failure

#### Update a Todo

- **URL**: `https://28-api.app.softgen.ai/api/todos/:id`
- **Method**: `PUT`
- **URL Parameters**: `id=[integer]` (required)
- **Request Body**: 
  ```json
  {
    "title": "string",
    "description": "string",
    "priority": "low | medium | high",
    "status": "pending | completed",
    "tags": ["string"]
  }
  ```
- **Response**: 
  ```json
  {
    "id": "number",
    "title": "string",
    "description": "string",
    "priority": "string",
    "status": "string",
    "tags": ["string"],
    "createdAt": "datetime",
    "updatedAt": "datetime"
  }
  ```
- **Status Codes**: 
  - `200 OK` on success
  - `500 Internal Server Error` on failure

#### Delete a Todo

- **URL**: `https://28-api.app.softgen.ai/api/todos/:id`
- **Method**: `DELETE`
- **URL Parameters**: `id=[integer]` (required)
- **Response**: No content
- **Status Codes**: 
  - `204 No Content` on success
  - `500 Internal Server Error` on failure

## Authentication

No authentication is required to access the current API endpoints.

## Error Handling

The API uses standard HTTP status codes to indicate the success or failure of an API request. In case of an error, the response will include a JSON object with a `message` field containing a description of the error.

## Examples

### Create a Todo

```bash
curl -X POST 'https://28-api.app.softgen.ai/api/todos' \
     -H 'Content-Type: application/json' \
     -d '{
       "title": "Grocery shopping",
       "description": "Buy milk, bread, and eggs",
       "priority": "low",
       "status": "pending",
       "tags": ["shopping", "errands"]
     }'
```

### Get All Todos

```bash
curl -X GET 'https://28-api.app.softgen.ai/api/todos'
```

### Update a Todo

```bash
curl -X PUT 'https://28-api.app.softgen.ai/api/todos/1' \
     -H 'Content-Type: application/json' \
     -d '{
       "title": "Prepare presentation",
       "description": "Slides for Monday meeting",
       "priority": "high",
       "status": "pending",
       "tags": ["work", "urgent"]
     }'
```

### Delete a Todo

```bash
curl -X DELETE 'https://28-api.app.softgen.ai/api/todos/1'
```

**Note**: Replace `1` with the actual `id` of the Todo you wish to delete.

## Conclusion

This document provides a comprehensive guide to the backend API for frontend developers. It includes all necessary details to understand and integrate with the backend services effectively.