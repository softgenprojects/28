import create from 'zustand';

export const useTodosStore = create((set) => ({
  todos: [],
  addTodo: (todo) => set((state) => ({ todos: [...state.todos, {
    id: todo.id,
    title: todo.title,
    description: todo.description,
    priority: todo.priority,
    status: todo.status,
    tags: todo.tags
  }] })),
  updateTodo: (id, updatedTodo) => set((state) => ({
    todos: state.todos.map((todo) => todo.id === id ? { ...todo, ...updatedTodo } : todo)
  })),
  removeTodo: (id) => set((state) => ({
    todos: state.todos.filter((todo) => todo.id !== id)
  }))
}));