import axios from 'axios';

const BASE_URL = 'https://28-api.app.softgen.ai/api/todos';

export const createTodo = async (todoData) => {
  try {
    const response = await axios.post(`${BASE_URL}`, todoData);
    return response.data;
  } catch (error) {
    throw new Error(error.response?.data || 'Error creating todo.');
  }
};

export const getAllTodos = async () => {
  try {
    const response = await axios.get(`${BASE_URL}`);
    return response.data;
  } catch (error) {
    throw new Error(error.response?.data || 'Error retrieving todos.');
  }
};

export const updateTodo = async (todoId, updateData) => {
  try {
    const response = await axios.put(`${BASE_URL}/${todoId}`, updateData);
    return response.data;
  } catch (error) {
    throw new Error(error.response?.data || 'Error updating todo.');
  }
};

export const deleteTodo = async (todoId) => {
  try {
    const response = await axios.delete(`${BASE_URL}/${todoId}`);
    return response.data;
  } catch (error) {
    throw new Error(error.response?.data || 'Error deleting todo.');
  }
};