import React from 'react';
import { useForm } from 'react-hook-form';
import { Box, Button, FormControl, FormLabel, Input, VStack } from '@chakra-ui/react';
import { TodoList } from '@components/TodoList';
import { useTodosStore } from '@store/useTodosStore';

const TodosPage = () => {
  const { register, handleSubmit, reset } = useForm();
  const { addTodo } = useTodosStore();

  const onSubmit = (data) => {
    addTodo({
      id: Date.now(),
      title: data.todo,
      description: '',
      priority: 'normal',
      status: 'pending',
      tags: []
    });
    reset();
  };

  return (
    <VStack spacing={8} p={5}>
      <Box w='full' maxW='md' mx='auto'>
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormControl isRequired>
            <FormLabel htmlFor='todo'>New Todo</FormLabel>
            <Input id='todo' placeholder='Enter your todo' {...register('todo')} />
          </FormControl>
          <Button mt={4} colorScheme='blue' type='submit'>Add Todo</Button>
        </form>
      </Box>
      <TodoList />
    </VStack>
  );
};

export default TodosPage;
