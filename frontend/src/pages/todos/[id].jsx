import { Box, Button, FormControl, FormLabel, Input, Textarea, useToast, VStack, Spinner, Center } from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { TodoItem } from '@components/TodoItem';
import { useRouter } from 'next/router';
import { useTodos } from '@hooks/useTodos';

const TodoPage = () => {
  const router = useRouter();
  const { id } = router.query;
  const { todos, updateTodo, isLoading } = useTodos();
  const todo = todos.find((todo) => todo.id === id);
  const toast = useToast();
  const { register, handleSubmit, reset } = useForm({
    defaultValues: {
      title: todo?.title,
      description: todo?.description,
    },
  });

  const onSubmit = async (data) => {
    await updateTodo(id, data);
    toast({
      title: 'Todo Updated',
      description: 'Your todo has been updated successfully.',
      status: 'success',
      duration: 2000,
      isClosable: true,
    });
    reset();
  };

  if (isLoading) {
    return (
      <Center p={4}>
        <Spinner />
      </Center>
    );
  }

  if (!todo) {
    return (
      <Center p={4}>
        <Box>Todo not found. Please check if the ID is correct or go back to the todos list.</Box>
      </Center>
    );
  }

  return (
    <VStack spacing={8} p={4}>
      <TodoItem todo={todo} />
      <Box as='form' onSubmit={handleSubmit(onSubmit)} w='full'>
        <FormControl id='title' isRequired>
          <FormLabel>Title</FormLabel>
          <Input placeholder='Title' {...register('title')} />
        </FormControl>
        <FormControl id='description' mt={4}>
          <FormLabel>Description</FormLabel>
          <Textarea placeholder='Description' {...register('description')} />
        </FormControl>
        <Button mt={4} colorScheme='blue' type='submit'>Update Todo</Button>
      </Box>
    </VStack>
  );
};

export default TodoPage;