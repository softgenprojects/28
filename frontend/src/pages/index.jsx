import { Box, VStack, Heading, Button, Text, useColorModeValue, Center } from '@chakra-ui/react';
import { BrandLogo } from '@components/BrandLogo';
import NextLink from 'next/link';

const Home = () => {
  const bgColor = useColorModeValue('gray.50', 'gray.800');
  const textColor = useColorModeValue('gray.800', 'whiteAlpha.900');

  return (
    <Center bg={bgColor} color={textColor} minH="100vh" py="12" px={{ base: '4', lg: '8' }}>
      <VStack spacing="8">
        <BrandLogo />
        <Heading as="h1" size="xl" textAlign="center">
          Welcome to SoftTodo!
        </Heading>
        <Text textAlign="center">
          Specialized Software Todo Planning App.
        </Text>
        <NextLink href="/" passHref>
          <Button colorScheme="pink" size="lg">
            Start
          </Button>
        </NextLink>
        <NextLink href="/todos" passHref>
          <Button colorScheme="teal" size="lg">
            Go to Todos
          </Button>
        </NextLink>
      </VStack>
    </Center>
  );
};

export default Home;