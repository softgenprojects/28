import React from 'react';
import { Box, List, ListItem, Text } from '@chakra-ui/react';
import { useTodos } from '@hooks/useTodos';

export const TodoList = () => {
  const { todos } = useTodos();

  return (
    <Box p={5} shadow='md' borderWidth='1px'>
      <Text fontSize='xl' mb={4}>SoftTodo - Software Todo Planning</Text>
      <List spacing={3}>
        {todos.map((todo) => (
          <ListItem key={todo.id} p={2} shadow='xs' borderWidth='1px'>
            {todo.title}
          </ListItem>
        ))}
      </List>
    </Box>
  );
};
