import React from 'react';
import { Box, Text, Button, Flex, useToast } from '@chakra-ui/react';
import { EditIcon, DeleteIcon } from '@chakra-ui/icons';
import { useTodos } from '@hooks/useTodos';
import PropTypes from 'prop-types';

export const TodoItem = ({ todo }) => {
  const { updateTodo, deleteTodo } = useTodos();
  const toast = useToast();

  if (!todo) {
    return (
      <Box p={4} borderWidth='1px' borderRadius='lg' overflow='hidden'>
        <Text fontWeight='bold'>Todo item not available</Text>
      </Box>
    );
  }

  const handleEdit = () => {
    toast({
      title: 'Edit Todo',
      description: `Editing todo with id: ${todo.id}`,
      status: 'info',
      duration: 2000,
      isClosable: true,
    });
  };

  const handleDelete = async () => {
    await deleteTodo(todo.id);
    toast({
      title: 'Todo Deleted',
      description: `You have deleted the todo with id: ${todo.id}`,
      status: 'success',
      duration: 2000,
      isClosable: true,
    });
  };

  return (
    <Box p={4} borderWidth='1px' borderRadius='lg' overflow='hidden'>
      <Flex justify='space-between' align='center'>
        <Box>
          <Text fontWeight='bold'>{todo.title}</Text>
          <Text fontSize='sm'>{todo.description}</Text>
        </Box>
        <Box>
          <Button leftIcon={<EditIcon />} colorScheme='blue' size='sm' onClick={handleEdit} mr={2}>
            Edit
          </Button>
          <Button leftIcon={<DeleteIcon />} colorScheme='red' size='sm' onClick={handleDelete}>
            Delete
          </Button>
        </Box>
      </Flex>
    </Box>
  );
};

TodoItem.propTypes = {
  todo: PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string,
    description: PropTypes.string,
  })
};
