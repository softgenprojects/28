import { Text, HStack } from '@chakra-ui/react';
import { GrStatusPlaceholderSmall } from 'react-icons/gr';

export const BrandLogo = () => (
  <HStack spacing={2} alignItems='center'>
    <GrStatusPlaceholderSmall size='32px' />
    <Text fontSize='xl' fontWeight='bold'>
      YourApp.com
    </Text>
  </HStack>
);