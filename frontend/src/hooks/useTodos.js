import { useEffect, useState, useCallback } from 'react';
import { useToast } from '@chakra-ui/react';
import { useTodosStore } from '@store/useTodosStore';
import { createTodo, getAllTodos, updateTodo, deleteTodo } from '@api/todosApi';

export const useTodos = () => {
  const toast = useToast();
  const { todos, addTodo, updateTodo: updateTodoInStore, removeTodo } = useTodosStore();
  const [isMounted, setIsMounted] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setIsMounted(true);
    const fetchTodos = async () => {
      try {
        const todosData = await getAllTodos();
        if (isMounted) {
          todosData.forEach((todo) => addTodo(todo));
          setIsLoading(false);
        }
      } catch (error) {
        toast({
          title: 'Error fetching todos',
          description: error.message,
          status: 'error',
          duration: 9000,
          isClosable: true,
        });
        setIsLoading(false);
      }
    };
    fetchTodos();
    return () => setIsMounted(false);
  }, []);

  const handleCreateTodo = useCallback(async (todoData) => {
    try {
      const newTodo = await createTodo(todoData);
      if (isMounted) {
        addTodo(newTodo);
      }
    } catch (error) {
      toast({
        title: 'Error creating todo',
        description: error.message,
        status: 'error',
        duration: 9000,
        isClosable: true,
      });
    }
  }, [isMounted]);

  const handleUpdateTodo = useCallback(async (todoId, todoData) => {
    try {
      const updatedTodo = await updateTodo(todoId, todoData);
      if (isMounted) {
        updateTodoInStore(todoId, updatedTodo);
      }
    } catch (error) {
      toast({
        title: 'Error updating todo',
        description: error.message,
        status: 'error',
        duration: 9000,
        isClosable: true,
      });
    }
  }, [isMounted]);

  const handleDeleteTodo = useCallback(async (todoId) => {
    try {
      await deleteTodo(todoId);
      if (isMounted) {
        removeTodo(todoId);
      }
    } catch (error) {
      toast({
        title: 'Error deleting todo',
        description: error.message,
        status: 'error',
        duration: 9000,
        isClosable: true,
      });
    }
  }, [isMounted]);

  return {
    todos,
    isLoading,
    createTodo: handleCreateTodo,
    updateTodo: handleUpdateTodo,
    deleteTodo: handleDeleteTodo
  };
};