const express = require('express');
const router = express.Router();
const { createTodoHandler, getTodosHandler, updateTodoHandler, deleteTodoHandler } = require('@controllers/TodoController');

router.post('/todos', createTodoHandler);
router.get('/todos', getTodosHandler);
router.put('/todos/:id', updateTodoHandler);
router.delete('/todos/:id', deleteTodoHandler);

module.exports = router;