const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

async function createTodo(todoData) {
  try {
    const todo = await prisma.todo.create({
      data: {
        title: todoData.title,
        description: todoData.description,
        priority: todoData.priority,
        status: todoData.status,
        tags: todoData.tags,
      },
    });
    return todo;
  } catch (error) {
    throw error;
  }
}

async function getTodos() {
  try {
    const todos = await prisma.todo.findMany();
    return todos;
  } catch (error) {
    throw error;
  }
}

async function updateTodo(id, updateData) {
  try {
    const todo = await prisma.todo.update({
      where: { id: parseInt(id, 10) },
      data: updateData,
    });
    return todo;
  } catch (error) {
    throw error;
  }
}

async function deleteTodo(id) {
  try {
    const todo = await prisma.todo.delete({
      where: { id: parseInt(id, 10) },
    });
    return todo;
  } catch (error) {
    throw error;
  }
}

module.exports = {
  createTodo,
  getTodos,
  updateTodo,
  deleteTodo
};