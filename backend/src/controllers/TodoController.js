const expressAsyncHandler = require('express-async-handler');
const { createTodo, getTodos, updateTodo, deleteTodo } = require('@services/TodoService');

const createTodoHandler = expressAsyncHandler(async (req, res) => {
  try {
    const todo = await createTodo(req.body);
    res.status(201).json(todo);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

const getTodosHandler = expressAsyncHandler(async (req, res) => {
  try {
    const todos = await getTodos();
    res.status(200).json(todos);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

const updateTodoHandler = expressAsyncHandler(async (req, res) => {
  try {
    const { id } = req.params;
    const todo = await updateTodo(id, req.body);
    res.status(200).json(todo);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

const deleteTodoHandler = expressAsyncHandler(async (req, res) => {
  try {
    const { id } = req.params;
    await deleteTodo(id);
    res.status(204).end();
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

module.exports = {
  createTodoHandler,
  getTodosHandler,
  updateTodoHandler,
  deleteTodoHandler
};