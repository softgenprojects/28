const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

async function main() {
  // Seed data for Todo model
  const todos = [
    {
      title: 'Grocery shopping',
      description: 'Buy milk, bread, and eggs',
      priority: 'low',
      status: 'pending',
      dueDate: new Date('2023-04-15T09:00:00Z'),
      tags: ['shopping', 'errands']
    },
    {
      title: 'Prepare presentation',
      description: 'Slides for Monday meeting',
      priority: 'high',
      status: 'pending',
      dueDate: new Date('2023-04-10T17:00:00Z'),
      tags: ['work', 'urgent']
    },
    {
      title: 'Book doctor appointment',
      description: 'Annual health checkup',
      priority: 'medium',
      status: 'completed',
      tags: ['health']
    },
    {
      title: 'Plan weekend getaway',
      description: 'Look for accommodation and activities',
      priority: 'medium',
      status: 'pending',
      tags: ['leisure', 'travel']
    }
  ];

  for (const todo of todos) {
    await prisma.todo.create({
      data: todo
    }).catch(error => {
      console.error('Error seeding Todo:', error);
    });
  }
}

main()
  .catch(e => {
    throw e;
  })
  .finally(async () => {
    await prisma.$disconnect();
  });